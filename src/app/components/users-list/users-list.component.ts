import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/User.interfaces';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  @Input() users: Array<User> = [];
  @Output() userIndex: EventEmitter<number> = new EventEmitter();

  public userSelected: number;
  constructor() { }

  ngOnInit() {}

  public userSelectedIndex(i) {
    if (i !== undefined) {
      console.log('USER LIST SELECT ' + i);
      this.userSelected = i;
      this.userIndex.emit(i);
    }
  }

}
