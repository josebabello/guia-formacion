import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/User.interfaces';

@Component({
  selector: 'app-current-user',
  templateUrl: './current-user.component.html',
  styleUrls: ['./current-user.component.scss']
})
export class CurrentUserComponent implements OnInit {
  @Input() user: User;
  constructor() { }

  ngOnInit() {
    console.log(this.user);
  }

}
