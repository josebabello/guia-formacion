import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styleUrls: ['./basics.component.scss']
})
export class BasicsComponent implements OnInit {
  public isDisabled: boolean = true;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.isDisabled = !this.isDisabled;
    }, 2000);
  }

  public btnClick() {
    alert('Button click');
  }
}
