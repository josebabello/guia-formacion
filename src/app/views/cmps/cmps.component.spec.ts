import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmpsComponent } from './cmps.component';

describe('CmpsComponent', () => {
  let component: CmpsComponent;
  let fixture: ComponentFixture<CmpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
