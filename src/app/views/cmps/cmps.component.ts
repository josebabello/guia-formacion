import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/models/User.interfaces';
import { Subscription } from 'rxjs';
import Swiper from 'swiper';
@Component({
  selector: 'app-cmps',
  templateUrl: './cmps.component.html',
  styleUrls: ['./cmps.component.scss']
})
export class CmpsComponent implements OnInit, OnDestroy {
  public users: Array<User> = [];
  public userSelected: User;
  public swiper: Swiper;

  private _urlUsers = 'https://randomuser.me/api/?results=5';
  private _usersObservable: Subscription;
  constructor(private _http: HttpClient) { }

  ngOnInit() {
    this._usersObservable = this._http.get(this._urlUsers).subscribe((data: any) => {
      this.users = data.results;
    });

    this.swiper = new Swiper('.swiper-container',{
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  }

  ngOnDestroy() {
    this._usersObservable.unsubscribe();
  }

  public getUserIndex($event) {
    console.log('CMPS ' + $event);
    this.userSelected = this.users[$event];
  }

}
