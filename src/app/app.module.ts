import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: BasicsComponent},
  { path: 'basics', component: BasicsComponent},
  { path: 'components', component: CmpsComponent},
  { path: 'modules', component: ModulesComponent },
  { path: 'pipes', component: ModulesComponent },
  { path: 'directives', component: ModulesComponent },
  { path: 'routing', component: ModulesComponent },
  { path: 'di', component: ModulesComponent },
  { path: 'observables', component: ModulesComponent },
  { path: '**', component: BasicsComponent }
];

import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { ContentComponent } from './components/content/content.component';
import { CmpsComponent } from './views/cmps/cmps.component';
import { ModulesComponent } from './views/modules/modules.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { CurrentUserComponent } from './components/current-user/current-user.component';
import { HttpClientModule } from '@angular/common/http';
import { BasicsComponent } from './views/basics/basics.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ContentComponent,
    CmpsComponent,
    ModulesComponent,
    UsersListComponent,
    CurrentUserComponent,
    BasicsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
